#Licences Report for the App

## Gradle Tasks

 * checkLicenses: check the the licenses.yml contains all dependency licenses with all needed properties 
 * updateLicences: update the licenses.yal.
 * generateLicencePage generate a licenses.html page from the licenses.yml
 
## Errors if data not complete

### Missing Libraries

If Libraries are missing the checkLicenses task fails with the following error

```
Execution failed for task ':app:checkLicenses'.
> checkLicenses: missing libraries in licenses.yml
```

To Solve this issue, do following steps:
 
 * run gradle updateLicences
 * fill missing properties in the licenses.yml

### Missing Required Properties
If required Properties missing the generateLicensePage task fails with the following error:

```
Execution failed for task ':app:generateLicensePage'.
> com.cookpad.android.plugin.license.exception.NotEnoughInformationException (no error message)
```

To Solve this issue, do following steps:
 
 * fill missing properties in the licenses.yml
