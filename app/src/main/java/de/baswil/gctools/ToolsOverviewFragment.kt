package de.baswil.gctools

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView

class ToolsOverviewFragment : Fragment(R.layout.tools_overview_fragment) {

    private val viewModel: ToolsOverviewViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)

        val recyclerView: RecyclerView = view.findViewById(R.id.tools_recycler_view)
        val adapter = ToolsRecycleViewAdapter(requireContext())
        recyclerView.adapter = adapter

        viewModel.tools.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }
    }

    override fun onResume() {
        super.onResume()

        val activity = requireActivity() as AppCompatActivity
        val actionBar: ActionBar = activity.supportActionBar!!
        actionBar.setTitle(R.string.app_name)
        actionBar.subtitle = ""
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.global_actionbar_search_menu, menu)
        inflater.inflate(R.menu.global_actionbar_menu, menu)

        val searchMenuItem = menu.findItem(R.id.search)
        val searchView = searchMenuItem.actionView as SearchView
        searchView.setIconifiedByDefault(true)
        searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = false

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.submitSearch(newText)
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.global_settings_menu_item) {
            val findNavController = findNavController()
            findNavController.navigate(R.id.action_startFragment_to_globalSettingsFragment)
            return true
        } else if (item.itemId == R.id.about_menu_item) {
            val findNavController = findNavController()
            findNavController.navigate(R.id.action_startFragment_to_aboutFragment)
            return true
        }

        return super.onOptionsItemSelected(item)
    }


}