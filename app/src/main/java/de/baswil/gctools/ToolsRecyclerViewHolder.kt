package de.baswil.gctools

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.app.BundleCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import de.baswil.gctools.tools.api.Tool

class ToolsRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val myTextView: TextView = itemView.findViewById(R.id.info_text)

    fun initTool(tool: Tool) {
        itemView.setOnClickListener {
//            Navigation.findNavController(itemView).navigate(tool.getNavigationGraphId())
            val arguments = Bundle()
            arguments.putSerializable("tool", tool)
            Navigation.findNavController(itemView).navigate(R.id.action_startFragment_to_toolFragment, arguments)
        }
        this.myTextView.text = itemView.context.getString(tool.getTitleId())
    }
}