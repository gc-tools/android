package de.baswil.gctools

import android.app.Application
import androidx.lifecycle.*
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.base64.Base64Tool
import de.baswil.gctools.tools.caesarcipher.CaesarCipherTool
import de.baswil.gctools.tools.morsecode.MorseCodeTool
import de.baswil.gctools.tools.romannumber.RomanNumberTool
import de.baswil.gctools.tools.wordvalue.WordValueTool
import java.text.Normalizer

class ToolsOverviewViewModel(application: Application) : AndroidViewModel(application) {
    private val allTools: List<Tool> = listOf(
        CaesarCipherTool(),
        WordValueTool(),
        Base64Tool(),
        MorseCodeTool(),
        RomanNumberTool(),
    ).sortedBy {
        application.getString(it.getTitleId())
    }

    private val searchTerm: MutableLiveData<String> = MutableLiveData(null)

    val tools: LiveData<List<Tool>> = Transformations.map(searchTerm) { searchTerm ->
        if (searchTerm == null || searchTerm.isEmpty()) {
            allTools
        } else {
            val replaceRegex = "\\p{InCombiningDiacriticalMarks}+".toRegex()
            val normalizedSearchTerm = Normalizer.normalize(searchTerm, Normalizer.Form.NFD)
                .replace(replaceRegex, "")
            allTools.filter { tool ->
                val toolName = application.getString(tool.getTitleId())
                val normalizedToolName = Normalizer.normalize(toolName, Normalizer.Form.NFD)
                    .replace(replaceRegex, "")
                normalizedToolName.contains(normalizedSearchTerm, true)
            }
        }
    }


    fun submitSearch(query: String?) {
        searchTerm.value = query
    }
}