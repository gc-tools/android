package de.baswil.gctools.about

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import de.baswil.gctools.BuildConfig
import de.baswil.gctools.R
import de.baswil.gctools.utils.preferances.setWebViewDarkTheme


class AboutFragment: Fragment(R.layout.about_fragment) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val ownLicenceWebView = view.findViewById<WebView>(R.id.own_license)!!
        setWebViewDarkTheme(requireContext(), ownLicenceWebView)
        ownLicenceWebView.loadUrl("file:///android_asset/own-license.html")

        val licencesWebView = view.findViewById<WebView>(R.id.third_party_library_licenses)!!
        setWebViewDarkTheme(requireContext(), licencesWebView)
        licencesWebView.loadUrl("file:///android_asset/licenses.html")

        val versionTextView = view.findViewById<TextView>(R.id.version)!!
        val versionString = BuildConfig.VERSION_NAME + if (BuildConfig.DEBUG) " DEBUG" else ""
        versionTextView.text = versionString
    }

    override fun onResume() {
        val activity = requireActivity() as AppCompatActivity
        val actionBar: ActionBar = activity.supportActionBar!!
        actionBar.setTitle(R.string.app_name)
        actionBar.setSubtitle(R.string.about_title)

        super.onResume()
    }
}