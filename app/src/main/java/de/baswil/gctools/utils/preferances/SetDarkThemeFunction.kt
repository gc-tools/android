package de.baswil.gctools.utils.preferances

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import de.baswil.gctools.R

fun setDarkTheme(context: Context) {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    val darkThemePreferenceValue = preferences.getString(
        context.getString(R.string.global_settings_theme_key),
        context.getString(R.string.global_settings_theme_default_value)
    ) ?: context.getString(R.string.global_settings_theme_default_value)

    when (darkThemePreferenceValue) {
        context.getString(R.string.global_settings_theme_system_value) -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
        context.getString(R.string.global_settings_theme_light_value) -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        context.getString(R.string.global_settings_theme_dark_value) -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }
}
