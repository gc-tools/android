package de.baswil.gctools.utils.preferances

import android.content.SharedPreferences
import androidx.lifecycle.LiveData

fun SharedPreferences.setInt(key: String, value: Int) {
    this.edit().putInt(key, value).apply()
}

fun SharedPreferences.setString(key: String, value: String) {
    this.edit().putString(key, value).apply()
}

fun SharedPreferences.setBoolean(key: String, value: Boolean) {
    this.edit().putBoolean(key, value).apply()
}

fun SharedPreferences.setFloatLive(key: String, value: Float) {
    this.edit().putFloat(key, value).apply()
}

fun SharedPreferences.setLong(key: String, value: Long) {
    this.edit().putLong(key, value).apply()
}

fun SharedPreferences.setStringSet(key: String, value: Set<String>) {
    this.edit().putStringSet(key, value).apply()
}