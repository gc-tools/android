package de.baswil.gctools.utils.clipboard

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat


fun copyToClipboard(context: Context, text: CharSequence, label: CharSequence? = null) {
    val clipboard: ClipboardManager? = ContextCompat.getSystemService(context, ClipboardManager::class.java)
    val clip = ClipData.newPlainText(label, text)
    clipboard?.setPrimaryClip(clip)
}

fun TextView.copyToClipboard() {
    copyToClipboard(context, text, null)
}

fun TextView.pasteFromClipboard() {
    val clipboard: ClipboardManager? = ContextCompat.getSystemService(context, ClipboardManager::class.java)
    val primaryClip = clipboard?.primaryClip
    if (primaryClip != null) {
        if (primaryClip.itemCount > 0) {
            text = primaryClip.getItemAt(0).text
        }
    }
}