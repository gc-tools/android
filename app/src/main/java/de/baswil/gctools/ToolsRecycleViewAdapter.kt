package de.baswil.gctools

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.baswil.gctools.tools.api.Tool
import java.util.*


class ToolsRecycleViewAdapter(context: Context) : RecyclerView.Adapter<ToolsRecyclerViewHolder>() {
    private var tools: List<Tool> = emptyList()
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToolsRecyclerViewHolder {
        val view = mInflater.inflate(R.layout.tool_item, parent, false)
        return ToolsRecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: ToolsRecyclerViewHolder, position: Int) {
        holder.initTool(tools[position])
    }

    override fun getItemCount(): Int {
        return this.tools.size
    }

    fun setData(tools: List<Tool>) {
        this.tools = tools
        notifyDataSetChanged()
    }
}