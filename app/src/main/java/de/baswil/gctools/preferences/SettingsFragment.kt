package de.baswil.gctools.preferences

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import de.baswil.gctools.R
import de.baswil.gctools.utils.preferances.setDarkTheme

class SettingsFragment : PreferenceFragmentCompat(), OnSharedPreferenceChangeListener {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.global_preferences, rootKey)
    }

    override fun onResume() {
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        super.onResume()

        val activity = requireActivity() as AppCompatActivity
        val actionBar: ActionBar = activity.supportActionBar!!
        actionBar.setTitle(R.string.app_name)
        actionBar.setSubtitle(R.string.global_settings_title)
    }

    override fun onPause() {
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)

        super.onPause()
    }


    override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String) {
        val context = requireContext()
        if (key == context.getString(R.string.global_settings_theme_key)) {
            setDarkTheme(context)
        }
    }
}