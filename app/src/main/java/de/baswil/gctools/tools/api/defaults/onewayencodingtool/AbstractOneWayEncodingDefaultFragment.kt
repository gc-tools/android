package de.baswil.gctools.tools.api.defaults.onewayencodingtool

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import de.baswil.gctools.R
import de.baswil.gctools.utils.clipboard.copyToClipboard
import de.baswil.gctools.utils.clipboard.pasteFromClipboard


abstract class AbstractOneWayEncodingDefaultFragment<T : OneWayEncodingDefaultFragmentViewModel> : Fragment() {

    protected lateinit var viewModel: T

    protected lateinit var input: EditText
    protected lateinit var inputContainer: TextInputLayout
    protected lateinit var output: EditText
    protected lateinit var outputContainer: TextInputLayout
    protected lateinit var pastButton: Button
    protected lateinit var copyButton: Button
    protected lateinit var deleteButton: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(getLayoutId(), container, false)
        viewModel = createViewModel()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        input = view.findViewById(R.id.tool_input)!!
        inputContainer = view.findViewById(R.id.tool_input_container)!!
        output = view.findViewById(R.id.tool_output)!!
        outputContainer = view.findViewById(R.id.tool_output_container)!!
        pastButton = view.findViewById(R.id.past_button)
        copyButton = view.findViewById(R.id.copy_button)
        deleteButton = view.findViewById(R.id.delete_button)

        setupInputText()
        setupOutputText()
        setupContainer()
        setupButtons()
    }


    private fun setupButtons() {
        this.pastButton.setOnClickListener {
            this.input.pasteFromClipboard()
        }

        this.copyButton.setOnClickListener {
            this.output.copyToClipboard()
            Toast.makeText(context, R.string.message_copy_result, Toast.LENGTH_SHORT).show()
        }

        this.deleteButton.setOnClickListener {
            this.input.setText("")
        }
    }

    private fun setupContainer() {
        this.inputContainer.hint = getString(getPlainTextLabelId())
        this.outputContainer.hint = getString(getCipherTextLabelId())
    }

    private fun setupInputText() {
        this.input.setText(viewModel.getInput().value)
        this.input.doAfterTextChanged { text -> viewModel.setInputValue(text.toString()) }
    }

    private fun setupOutputText() {
        this.output.keyListener = null
        this.output.setOnLongClickListener {
            this.output.copyToClipboard()
            Toast.makeText(context, R.string.message_copy_result, Toast.LENGTH_SHORT).show()
            return@setOnLongClickListener true
        }
        viewModel.getOutput().observe(viewLifecycleOwner) { text -> this.output.setText(text) }
    }

    abstract fun createViewModel(): T

    @LayoutRes
    open fun getLayoutId(): Int {
        return R.layout.tool_one_way_encoding_fragment
    }

    @StringRes
    open fun getPlainTextLabelId(): Int {
        return R.string.tool_one_way_plaintext_label
    }

    @StringRes
    open fun getCipherTextLabelId(): Int {
        return R.string.tool_one_way_ciphertext_label
    }
}