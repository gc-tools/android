package de.baswil.gctools.tools.api.defaults.twowayencodingtool

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.LayoutRes
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.onewayencodingtool.AbstractOneWayEncodingDefaultFragment


abstract class AbstractTwoWayEncodingDefaultFragment<T : TwoWayEncodingDefaultFragmentViewModel> : AbstractOneWayEncodingDefaultFragment<T>() {
    protected lateinit var changeModeButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeModeButton = view.findViewById(R.id.change_mode_button)

        setupContainer()
        setupButtons()
    }


    private fun setupButtons() {
        this.changeModeButton.setOnClickListener {
            val output = viewModel.getOutput().value ?: ""
            viewModel.toggleMode()
            this.input.setText(output)
        }
    }

    private fun setupContainer() {
        viewModel.getMode().observe(viewLifecycleOwner) {
            if (it == getString(getSettingsModeEncodeValueId())) {
                this.inputContainer.hint = getString(getPlainTextLabelId())
                this.outputContainer.hint = getString(getCipherTextLabelId())
            } else {
                this.inputContainer.hint = getString(getCipherTextLabelId())
                this.outputContainer.hint = getString(getPlainTextLabelId())
            }
        }
    }

    @LayoutRes
    override fun getLayoutId(): Int {
        return R.layout.tool_two_way_encoding_fragment
    }

    open fun getSettingsModeEncodeValueId(): Int {
        return R.string.two_way_settings_mode_encode_value
    }
}