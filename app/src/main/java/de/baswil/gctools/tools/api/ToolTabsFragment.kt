package de.baswil.gctools.tools.api

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import de.baswil.gctools.R


class ToolTabsFragment : Fragment(R.layout.tool_tabs_fragment) {

    private val viewModel: ToolTabsFragmentViewModel by viewModels()

    private lateinit var tool: Tool

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)

        tool = arguments?.getSerializable("tool") as Tool

        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        val viewPager = view.findViewById<ViewPager2>(R.id.pager)

        val toolTabAdapter = ToolTabsAdapter(this, tool)
        if (toolTabAdapter.itemCount == 1) {
            tabLayout.visibility = View.GONE
        }

        viewPager.adapter = toolTabAdapter

        // Workaround for issue:
        // - https://github.com/android/views-widgets-samples/issues/107
        // - https://github.com/material-components/material-components-android/issues/500
        // ToDo: Workaround
        viewPager.offscreenPageLimit = 1

        if (viewModel.getSelectedTab().value == null) {
            viewModel.setSelectedTab(toolTabAdapter.getToolTabPosition())
        }
        viewPager.setCurrentItem(viewModel.getSelectedTab().value!!, false)

        TabLayoutMediator(tabLayout, viewPager, true) { tab, position ->
            toolTabAdapter.setTabHeader(tab, position)
        }.attach()

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                view.clearFocus()
                val imm: InputMethodManager = getSystemService(requireContext(), InputMethodManager::class.java)!!
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        val activity = requireActivity() as AppCompatActivity
        val actionBar: ActionBar = activity.supportActionBar!!
        actionBar.setTitle(this.tool.getTitleId())
        actionBar.subtitle = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.global_actionbar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.global_settings_menu_item -> {
                val findNavController = findNavController()
                findNavController.navigate(R.id.action_startFragment_to_globalSettingsFragment)
                true
            }
            R.id.about_menu_item -> {
                val findNavController = findNavController()
                findNavController.navigate(R.id.action_startFragment_to_aboutFragment)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}