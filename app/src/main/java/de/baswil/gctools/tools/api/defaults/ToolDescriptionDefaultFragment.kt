package de.baswil.gctools.tools.api.defaults

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.annotation.StringRes
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.Fragment
import com.google.android.material.textview.MaterialTextView
import de.baswil.gctools.R

class ToolDescriptionDefaultFragment : Fragment(R.layout.tool_description_default_fragment) {

    companion object {
        const val ARGUMENT_DESCRIPTION_IDS = "ARGUMENT_DESCRIPTION_IDS"

        fun newInstance(@StringRes vararg descriptionTextIds: Int): ToolDescriptionDefaultFragment {
            val fragment = ToolDescriptionDefaultFragment()

            val args = Bundle()
            args.putIntArray(ARGUMENT_DESCRIPTION_IDS, descriptionTextIds)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var descriptionTextIds = getDescriptionIds()
        if (descriptionTextIds.isEmpty()) {
            descriptionTextIds = intArrayOf(R.string.no_description)
        }

        val descriptionContainer = view.findViewById<LinearLayoutCompat>(R.id.description_container)

        for ((index, descriptionTextId) in descriptionTextIds.withIndex()) {
            val textView = MaterialTextView(
                requireContext(),
                null,
                R.style.TextAppearance_MaterialComponents_Body1
            )
            textView.setText(descriptionTextId)
            textView.movementMethod = LinkMovementMethod.getInstance()

            val layoutParams = LinearLayoutCompat.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            val margin = resources.getDimensionPixelSize(R.dimen.shared_default_margin)
            layoutParams.setMargins(margin, if (index == 0) margin else 0, margin, margin)

            descriptionContainer.addView(textView, layoutParams)
        }
    }

    private fun getDescriptionIds(): IntArray {
        return try {
            val arguments = requireArguments()
            arguments.getIntArray(ARGUMENT_DESCRIPTION_IDS) ?: IntArray(0)
        } catch (e: IllegalStateException) {
            IntArray(0)
        }
    }
}
