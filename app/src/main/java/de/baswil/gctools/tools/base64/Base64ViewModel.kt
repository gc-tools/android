package de.baswil.gctools.tools.base64

import android.app.Application
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragmentViewModel

class Base64ViewModel(application: Application) : AbstractTwoWayEncodingDefaultFragmentViewModel(application) {

    private val decoder = Base64Decoder()
    private val encoder = Base64Encoder()

    override fun calculateOutput() {
        val text = inputLiveData.value
        val mode: String = modeLiveData.value
            ?: getApplication<Application>().getString(getModeSettingsDefaultValueId())

        if (mode == getApplication<Application>().getString(getModeSettingsDecodeValueId())) {
            outputLiveData.value = decoder.decode(text)
        } else if (mode == getApplication<Application>().getString(getModeSettingsEncodeValueId())) {
            outputLiveData.value = encoder.encode(text)
        }
    }

    override fun getModeSettingsKey(): Int {
        return R.string.base64_settings_mode_key
    }
}