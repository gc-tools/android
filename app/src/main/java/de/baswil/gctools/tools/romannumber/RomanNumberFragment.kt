package de.baswil.gctools.tools.romannumber

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragment


class RomanNumberFragment : AbstractTwoWayEncodingDefaultFragment<RomanNumberViewModel>() {
    override fun createViewModel(): RomanNumberViewModel {
        return ViewModelProvider(this).get(RomanNumberViewModel::class.java)
    }

    @StringRes
    override fun getPlainTextLabelId(): Int {
        return R.string.romannumber_normal_number
    }

    @StringRes
    override fun getCipherTextLabelId(): Int {
        return R.string.romannumber_roman_number
    }
}