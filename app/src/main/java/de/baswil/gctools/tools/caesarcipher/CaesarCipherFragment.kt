package de.baswil.gctools.tools.caesarcipher

import androidx.lifecycle.ViewModelProvider
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragment

class CaesarCipherFragment : AbstractTwoWayEncodingDefaultFragment<CaesarCipherViewModel>() {
    override fun createViewModel(): CaesarCipherViewModel {
        return ViewModelProvider(this).get(CaesarCipherViewModel::class.java)
    }
}