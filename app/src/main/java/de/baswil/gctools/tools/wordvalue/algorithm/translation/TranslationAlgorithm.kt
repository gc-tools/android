package de.baswil.gctools.tools.wordvalue.algorithm.translation

interface TranslationAlgorithm {
    fun translate(text: String): Int
}