package de.baswil.gctools.tools.wordvalue.algorithm.accent

interface AccentReplacementAlgorithm {
    fun replace(text: String): String
}