package de.baswil.gctools.tools.api

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import de.baswil.gctools.R

class ToolTabsAdapter(
    fragment: Fragment,
    val tool: Tool
) : FragmentStateAdapter(fragment) {

    private val fragmentTypes: Array<FragmentType> by lazy {
        val list = mutableListOf<FragmentType>()
        if (tool.isDescriptionFragmentEnabled()) {
            list.add(FragmentType.DESCRIPTION)
        }
        list.add(FragmentType.TOOL)
        if (tool.isPreferencesFragmentEnabled()) {
            list.add(FragmentType.PREFERENCES)
        }
        list.toTypedArray()
    }

    private val items: Int by lazy {
        fragmentTypes.size
    }

    override fun getItemCount(): Int {
        return items
    }

    override fun createFragment(position: Int): Fragment {
        return when (fragmentTypes[position]) {
            FragmentType.DESCRIPTION -> {
                createDescriptionFragment()
            }
            FragmentType.TOOL -> {
                createToolFragment()
            }
            FragmentType.PREFERENCES -> {
                createPreferenceFragment()
            }
        }
    }

    fun setTabHeader(tab: TabLayout.Tab, position: Int) {
        when (fragmentTypes[position]) {
            FragmentType.DESCRIPTION -> {
                setupDescriptionTab(tab)
            }
            FragmentType.TOOL -> {
                setupToolTab(tab)
            }
            FragmentType.PREFERENCES -> {
                setupPreferenceTab(tab)
            }
        }
    }



    private fun createPreferenceFragment(): Fragment {
        val settingsFragment = tool.createPreferencesFragment()!!
        tool.prepareFragmentArguments(settingsFragment, tool)
        return settingsFragment
    }

    private fun createToolFragment(): Fragment {
        val toolFragment = tool.createToolFragment()
        tool.prepareFragmentArguments(toolFragment, tool)
        return toolFragment
    }

    private fun createDescriptionFragment(): Fragment {
        val descriptionFragment = tool.createDescriptionFragment()!!
        tool.prepareFragmentArguments(descriptionFragment, tool)
        return descriptionFragment
    }

    private fun setupDescriptionTab(tab: TabLayout.Tab) {
        tab.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED
        tab.setText(R.string.tool_tab_label_description)
        tab.setContentDescription(R.string.tool_tab_label_description)
        tab.setIcon(R.drawable.ic_baseline_info_24)
    }

    private fun setupToolTab(tab: TabLayout.Tab) {
        tab.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED
        tab.setText(R.string.tool_tab_label_tool)
        tab.setContentDescription(R.string.tool_tab_label_tool)
        tab.setIcon(R.drawable.ic_baseline_construction_24)
    }

    private fun setupPreferenceTab(tab: TabLayout.Tab) {
        tab.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED
        tab.setText(R.string.tool_tab_label_settings)
        tab.setContentDescription(R.string.tool_tab_label_settings)
        tab.setIcon(R.drawable.ic_baseline_settings_24)
    }

    fun getToolTabPosition(): Int {
        return fragmentTypes.indexOf(FragmentType.TOOL)
    }

    enum class FragmentType {
        DESCRIPTION,
        TOOL,
        PREFERENCES
    }
}