package de.baswil.gctools.tools.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ToolTabsFragmentViewModel : ViewModel() {
    private val selectedTab: MutableLiveData<Int> = MutableLiveData(null)

    fun getSelectedTab(): LiveData<Int> = selectedTab
    fun setSelectedTab(selectedTab: Int) {
        this.selectedTab.value = selectedTab
    }
}