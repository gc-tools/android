package de.baswil.gctools.tools.romannumber

import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment
import de.baswil.gctools.tools.api.defaults.ToolSettingsDefaultFragment

class RomanNumberTool : Tool() {
    override fun getTitleId(): Int {
        return R.string.romannumber_title
    }

    override fun createToolFragment(): Fragment {
        return RomanNumberFragment()
    }

    override fun createPreferencesFragment(): Fragment? = null

    override fun isPreferencesFragmentEnabled(): Boolean = false

    override fun createDescriptionFragment(): Fragment {
        return ToolDescriptionDefaultFragment.newInstance(
            R.string.romannumber_description1,
            R.string.romannumber_description2,
            R.string.romannumber_description3,
        )
    }
}