package de.baswil.gctools.tools.caesarcipher

import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment
import de.baswil.gctools.tools.api.defaults.ToolSettingsDefaultFragment

class CaesarCipherTool : Tool() {
    override fun getTitleId(): Int {
        return R.string.caesarcipher_title
    }

    override fun createToolFragment(): Fragment {
        return CaesarCipherFragment()
    }

    override fun createPreferencesFragment(): Fragment {
        return ToolSettingsDefaultFragment.newInstance(R.xml.ceasarcipher_preferences)
    }

    override fun createDescriptionFragment(): Fragment {
        return ToolDescriptionDefaultFragment.newInstance(
            R.string.caesarcipher_description1,
            R.string.caesarcipher_description2,
            R.string.caesarcipher_description3,
        )
    }
}