package de.baswil.gctools.tools.api.defaults.onewayencodingtool

import androidx.lifecycle.LiveData

interface OneWayEncodingDefaultFragmentViewModel {
    fun getOutput(): LiveData<String>
    fun getInput(): LiveData<String>
    fun setInputValue(inputValue: String?)
}