package de.baswil.gctools.tools.morsecode

import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment
import de.baswil.gctools.tools.api.defaults.ToolSettingsDefaultFragment

class MorseCodeTool : Tool() {
    override fun getTitleId(): Int {
        return R.string.morsecode_title
    }

    override fun createToolFragment(): Fragment {
        return MorseCodeFragment()
    }

    override fun createPreferencesFragment(): Fragment? = null

    override fun isPreferencesFragmentEnabled() = false

    override fun createDescriptionFragment(): Fragment {
        return ToolDescriptionDefaultFragment.newInstance(
            R.string.morsecode_description1,
            R.string.morsecode_description2,
            R.string.morsecode_description3,
        )
    }
}