package de.baswil.gctools.tools.romannumber

import android.app.Application
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragmentViewModel

class RomanNumberViewModel(application: Application) : AbstractTwoWayEncodingDefaultFragmentViewModel(application) {
    private val decoder = RomanNumberDecoder()
    private val encoder = RomanNumberEncoder()

    override fun calculateOutput() {
        val text = inputLiveData.value
        val mode: String = modeLiveData.value
            ?: getApplication<Application>().getString(getModeSettingsDefaultValueId())

        if (mode == getApplication<Application>().getString(getModeSettingsDecodeValueId())) {
            outputLiveData.value = decoder.decode(text)
        } else if (mode == getApplication<Application>().getString(getModeSettingsEncodeValueId())) {
            outputLiveData.value = encoder.encode(text)
        }
    }

    override fun getModeSettingsKey(): Int {
        return R.string.romannumber_settings_mode_key
    }
}