package de.baswil.gctools.tools.caesarcipher

import android.app.Application
import androidx.lifecycle.*
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragmentViewModel
import de.baswil.gctools.utils.preferances.stringLiveData

class CaesarCipherViewModel(application: Application) : AbstractTwoWayEncodingDefaultFragmentViewModel(application) {
    private val decoder = CaesarCipherDecoder()
    private val encoder = CaesarCipherEncoder()

    private val keyLiveData: LiveData<String> = preferences.stringLiveData(
        application.getString(R.string.caesarcipher_setting_key_key),
        application.getString(R.string.caesarcipher_setting_key_default_values)
    )

    init {
        outputLiveData.addSource(keyLiveData) { calculateOutput() }
    }

    override fun calculateOutput() {
        val text = inputLiveData.value
        val key: Int = (keyLiveData.value
            ?: getApplication<Application>().getString(R.string.caesarcipher_setting_key_default_values))
            .toInt()
        val mode = modeLiveData.value
            ?: getApplication<Application>().getString(getModeSettingsDefaultValueId())

        if (mode == getApplication<Application>().getString(getModeSettingsDecodeValueId())) {
            outputLiveData.value = decoder.decode(text, key)
        } else if (mode == getApplication<Application>().getString(getModeSettingsEncodeValueId())) {
            outputLiveData.value = encoder.encode(text, key)
        }
    }

    override fun getModeSettingsKey(): Int {
        return R.string.caesarcipher_settings_mode_key
    }
}