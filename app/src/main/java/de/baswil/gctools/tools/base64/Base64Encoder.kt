package de.baswil.gctools.tools.base64

import android.util.Base64

class Base64Encoder {
    fun encode(string: String?): String {
        if (string == null || string.isEmpty()) {
            return "";
        }

        return Base64.encodeToString(string.toByteArray(), Base64.NO_WRAP)
    }
}