package de.baswil.gctools.tools.wordvalue.algorithm.accent

import android.content.Context
import de.baswil.gctools.R
import java.text.Normalizer

class AccentReplacementAlgorithmFactory(private val context: Context) {
    fun createAccentReplacer(accentReplacementType: String): AccentReplacementAlgorithm {
        return when (accentReplacementType) {
            context.getString(R.string.wordvalue_setting_accent_replacement_no_value) -> {
                NoAccentReplaceAlgorithm()
            }
            context.getString(R.string.wordvalue_setting_accent_replacement_base_character_value) -> {
                BaseCharacterAccentReplaceAlgorithm()
            }
            context.getString(R.string.wordvalue_setting_accent_replacement_german_character_value) -> {
                GermanAlternativeAccentReplaceAlgorithm()
            }
            else -> {
                NoAccentReplaceAlgorithm()
            }
        }
    }

    class NoAccentReplaceAlgorithm : AccentReplacementAlgorithm {
        override fun replace(text: String): String {
            return text
        }
    }

    class BaseCharacterAccentReplaceAlgorithm : AccentReplacementAlgorithm {
        override fun replace(text: String): String {
            return Normalizer
                .normalize(text, Normalizer.Form.NFD)
                .replace("[^\\p{ASCII}]".toRegex(), "")
        }
    }

    class GermanAlternativeAccentReplaceAlgorithm : AccentReplacementAlgorithm {
        override fun replace(text: String): String {
            return text
                .replace("ä".toRegex(RegexOption.IGNORE_CASE), "ae")
                .replace("ö".toRegex(RegexOption.IGNORE_CASE), "oe")
                .replace("ü".toRegex(RegexOption.IGNORE_CASE), "ue")
                .replace("ß".toRegex(RegexOption.IGNORE_CASE), "ss")
        }
    }
}