package de.baswil.gctools.tools.caesarcipher

class CaesarCipherEncoder : CaesarCipherBaseCoder(){
    fun encode(string: String?, key: Int): String {
        return code(string, key)
    }
}