package de.baswil.gctools.tools.api.defaults.twowayencodingtool

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.onewayencodingtool.AbstractOneWayEncodingDefaultFragmentViewModel
import de.baswil.gctools.utils.preferances.setString
import de.baswil.gctools.utils.preferances.stringLiveData

abstract class AbstractTwoWayEncodingDefaultFragmentViewModel(application: Application) :
    AbstractOneWayEncodingDefaultFragmentViewModel(application), TwoWayEncodingDefaultFragmentViewModel {

    protected val preferences = PreferenceManager.getDefaultSharedPreferences(application)

    protected val modeLiveData: LiveData<String> = preferences.stringLiveData(
        application.getString(this.getModeSettingsKey()),
        application.getString(this.getModeSettingsDefaultValueId())
    )

    init {
        outputLiveData.addSource(modeLiveData) { calculateOutput() }
    }

    override fun getMode(): LiveData<String> = this.modeLiveData

    override fun toggleMode() {
        val modeValue =
            if (modeLiveData.value == getApplication<Application>().getString(getModeSettingsEncodeValueId())) {
                getApplication<Application>().getString(getModeSettingsDecodeValueId())
            } else {
                getApplication<Application>().getString(getModeSettingsEncodeValueId())
            }

        preferences.setString(
            getApplication<Application>().getString(getModeSettingsKey()),
            modeValue
        )
    }

    @StringRes
    open fun getModeSettingsEncodeValueId() = R.string.two_way_settings_mode_encode_value

    @StringRes
    open fun getModeSettingsDecodeValueId() = R.string.two_way_settings_mode_decode_value

    @StringRes
    open fun getModeSettingsDefaultValueId() = R.string.two_way_settings_mode_default_value

    @StringRes
    abstract fun getModeSettingsKey(): Int
}