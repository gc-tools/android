package de.baswil.gctools.tools.wordvalue.algorithm.translation

import android.content.Context
import de.baswil.gctools.R
import java.util.*

class TranslationAlgorithmFactory(private val context: Context) {
    fun createTranslationAlgorithm(translationType: String): TranslationAlgorithm {
        return when (translationType) {
            context.getString(R.string.wordvalue_setting_translation_a1z26_value) -> {
                A1Z26TranslationAlgorithm()
            }
            context.getString(R.string.wordvalue_setting_translation_a26z1_value) -> {
                A26Z1TranslationAlgorithm()
            }
            else -> {
                A1Z26TranslationAlgorithm()
            }
        }
    }

    class A1Z26TranslationAlgorithm : TranslationAlgorithm {
        override fun translate(text: String): Int {
            return text
                .toLowerCase(Locale.getDefault())
                .replace("[^a-z]".toRegex(), "")
                .map { char -> char - 'a' + 1 }
                .sum()
        }
    }

    class A26Z1TranslationAlgorithm : TranslationAlgorithm {
        override fun translate(text: String): Int {
            return text
                .toLowerCase(Locale.getDefault())
                .replace("[^a-z]".toRegex(), "")
                .map { char -> (char - 'a' - 26) * -1 }
                .sum()
        }
    }
}