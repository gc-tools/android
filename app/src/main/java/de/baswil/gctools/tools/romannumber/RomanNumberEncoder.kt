package de.baswil.gctools.tools.romannumber

class RomanNumberEncoder {
    private val romanTranslateList: List<Pair<Int, String>> by lazy {
        listOf(
            Pair(1000, "M"),
            Pair(900, "CM"),
            Pair(500, "D"),
            Pair(400, "CD"),
            Pair(100, "C"),
            Pair(90, "XC"),
            Pair(50, "L"),
            Pair(40, "XL"),
            Pair(10, "X"),
            Pair(9, "IX"),
            Pair(5, "V"),
            Pair(4, "IV"),
            Pair(1, "I"),
        )
    }

    fun encode(string: String?): String {
        if (string == null || string.trim().isEmpty()) {
            return ""
        }
        var int = string.trim().toIntOrNull()
        if (int == null || int <= 0 || int >= 4000) {
            return ""
        }

        var result = ""

        for (translatePair in romanTranslateList) {
            val fullInts = int / translatePair.first
            if (fullInts > 0) {
                result += translatePair.second.repeat(fullInts)
                int -= translatePair.first * fullInts
            }
        }

        return result
    }
}