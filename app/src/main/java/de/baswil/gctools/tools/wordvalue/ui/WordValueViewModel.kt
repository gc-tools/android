package de.baswil.gctools.tools.wordvalue.ui

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import de.baswil.gctools.utils.preferances.booleanLiveData
import de.baswil.gctools.utils.preferances.stringLiveData
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.onewayencodingtool.AbstractOneWayEncodingDefaultFragmentViewModel
import de.baswil.gctools.tools.wordvalue.algorithm.WordValueAlgorithm

class WordValueViewModel(application: Application) :
    AbstractOneWayEncodingDefaultFragmentViewModel(application) {

    private val preferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(application)

    private val translationTypeLivaData: LiveData<String> = preferences.stringLiveData(
        application.getString(R.string.wordvalue_setting_translation_key),
        application.getString(R.string.wordvalue_setting_translation_default_value)
    )

    private val accentReplacementTypeLivaData: LiveData<String> = preferences.stringLiveData(
        application.getString(R.string.wordvalue_setting_accent_replacement_key),
        application.getString(R.string.wordvalue_setting_accent_replacement_default_value)
    )

    private val lineByLineLivaData: LiveData<Boolean> = preferences.booleanLiveData(
        application.getString(R.string.wordvalue_setting_line_by_line_key),
        application.resources.getBoolean(R.bool.wordvalue_setting_line_by_line_default_value)
    )

    init {
        outputLiveData.addSource(translationTypeLivaData) { calculateOutput() }
        outputLiveData.addSource(accentReplacementTypeLivaData) { calculateOutput() }
        outputLiveData.addSource(lineByLineLivaData) { calculateOutput() }
    }

    override fun calculateOutput() {
        val inputText = inputLiveData.value ?: ""
        val translationType = translationTypeLivaData.value
            ?: getApplication<Application>().getString(R.string.wordvalue_setting_translation_default_value)
        val accentReplacementType = accentReplacementTypeLivaData.value
            ?: getApplication<Application>().getString(R.string.wordvalue_setting_accent_replacement_default_value)
        val inputLineByLine = lineByLineLivaData.value
            ?: getApplication<Application>().resources.getBoolean(R.bool.wordvalue_setting_line_by_line_default_value)

        val calculateWordValues = WordValueAlgorithm(getApplication<Application>())
            .calculateWordValue(
                inputText,
                translationType,
                accentReplacementType,
                inputLineByLine
            )

        outputLiveData.value = calculateWordValues.joinToString(System.lineSeparator())
    }
}