package de.baswil.gctools.tools.api.defaults.onewayencodingtool

import android.app.Application
import androidx.lifecycle.*

abstract class AbstractOneWayEncodingDefaultFragmentViewModel(application: Application) :
    AndroidViewModel(application), OneWayEncodingDefaultFragmentViewModel {

    protected val inputLiveData: MutableLiveData<String> = MutableLiveData("")
    protected val outputLiveData: MediatorLiveData<String> = MediatorLiveData()

    init {
        outputLiveData.addSource(inputLiveData) { calculateOutput() }
    }

    override fun getOutput(): LiveData<String> = this.outputLiveData

    override fun getInput(): LiveData<String> = this.inputLiveData

    override fun setInputValue(inputValue: String?) {
        this.inputLiveData.value = inputValue ?: ""
    }

    abstract fun calculateOutput()
}