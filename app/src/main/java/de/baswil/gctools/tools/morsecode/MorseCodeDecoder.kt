package de.baswil.gctools.tools.morsecode

class MorseCodeDecoder {

    private val TRANSLATION_MAP: Map<String, String> by lazy {
        hashMapOf(
            Pair(".-", "A"),
            Pair("-...", "B"),
            Pair("-.-.", "C"),
            Pair("-..", "D"),
            Pair(".", "E"),
            Pair("..-.", "F"),
            Pair("--.", "G"),
            Pair("....", "H"),
            Pair("..", "I"),
            Pair(".---", "J"),
            Pair("-.-", "K"),
            Pair(".-..", "L"),
            Pair("--", "M"),
            Pair("-.", "N"),
            Pair("---", "O"),
            Pair(".--.", "P"),
            Pair("--.-", "Q"),
            Pair(".-.", "R"),
            Pair("...", "S"),
            Pair("-", "T"),
            Pair("..-", "U"),
            Pair("...-", "V"),
            Pair(".--", "W"),
            Pair("-..-", "X"),
            Pair("-.--", "Y"),
            Pair("--..", "Z"),
            Pair(".--.-", "À"),
            Pair(".--.-", "Å"),
            Pair(".-.-", "Ä"),
            Pair(".-..-", "È"),
            Pair("..-..", "É"),
            Pair("---.", "Ö"),
            Pair("..--", "Ü"),
            Pair("...--..", "ß"),
            Pair("----", "CH"),
            Pair("--.--", "Ñ"),
            Pair(".----", "1"),
            Pair("..---", "2"),
            Pair("...--", "3"),
            Pair("....-", "4"),
            Pair(".....", "5"),
            Pair("-....", "6"),
            Pair("--...", "7"),
            Pair("---..", "8"),
            Pair("----.", "9"),
            Pair("-----", "0"),
            Pair(".-.-.-", "."),
            Pair("--..--", ","),
            Pair("---...", ":"),
            Pair("-.-.-.", ";"),
            Pair("..--..", "?"),
            Pair("-.-.--", "!"),
            Pair("-....-", "-"),
            Pair("..--.-", "_"),
            Pair("-.--.", "("),
            Pair("-.--.-", ")"),
            Pair(".----.", "'"),
            Pair(".-..-.", "\""),
            Pair("-...-", "="),
            Pair(".-.-.", "+"),
            Pair("-..-.", "/"),
            Pair(".--.-.", "@"),
            Pair(".-...", "&"),
            Pair("/", " "),
        )
    }

    fun decode(string: String?): String {
        if (string == null || string.isEmpty()) {
            return ""
        }

        return string
            .replace("   ", " / ")
            .split(" ")
            .filter { it.isNotEmpty() }
            .joinToString("") {
                TRANSLATION_MAP[it] ?: "#"
            }
    }
}