package de.baswil.gctools.tools.wordvalue.algorithm

import android.content.Context
import de.baswil.gctools.tools.wordvalue.algorithm.accent.AccentReplacementAlgorithmFactory
import de.baswil.gctools.tools.wordvalue.algorithm.translation.TranslationAlgorithmFactory

class WordValueAlgorithm(private val context: Context) {
    fun calculateWordValue(
        text: String,
        translationType: String,
        accentReplacementType: String,
        lineByLine: Boolean,
    ): List<Int> {
        val accentReplacer = AccentReplacementAlgorithmFactory(context).createAccentReplacer(accentReplacementType)
        val translator = TranslationAlgorithmFactory(context).createTranslationAlgorithm(translationType)

        val lineList = if (lineByLine) {
            text.lines()
        } else {
            listOf(text)
        }

        return lineList
            .map { accentReplacer.replace(it) }
            .map { translator.translate(it) }
    }
}