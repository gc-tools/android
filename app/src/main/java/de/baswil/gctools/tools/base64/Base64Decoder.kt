package de.baswil.gctools.tools.base64

import android.util.Base64

class Base64Decoder {
    fun decode(string: String?): String {
        if (string == null || string.isEmpty()) {
            return "";
        }

        var result = ""

        for (tryCount in 0..2) {
            // Correction for missing padding
            val tryText =  string + "=".repeat(tryCount)
            try {
                result = String(Base64.decode(tryText, Base64.DEFAULT))
                break
            } catch (e: IllegalArgumentException) {
            }
        }

        return result
    }
}