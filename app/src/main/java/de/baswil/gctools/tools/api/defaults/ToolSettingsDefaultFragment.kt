package de.baswil.gctools.tools.api.defaults

import android.os.Bundle
import androidx.annotation.XmlRes
import androidx.preference.PreferenceFragmentCompat
import de.baswil.gctools.R

class ToolSettingsDefaultFragment : PreferenceFragmentCompat() {

    companion object {
        const val ARGUMENT_PREFERENCES_XML_ID = "ARGUMENT_PREFERENCES_XML_ID"

        fun newInstance(@XmlRes preferencesId: Int): ToolSettingsDefaultFragment {
            val fragment = ToolSettingsDefaultFragment()

            val args = Bundle()
            args.putInt(ARGUMENT_PREFERENCES_XML_ID, preferencesId)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val preferencesXmlId = getPreferenceXmlArgument()
        setPreferencesFromResource(preferencesXmlId, rootKey)
    }

    @XmlRes
    private fun getPreferenceXmlArgument(): Int {
        return try {
            val arguments = requireArguments()
            arguments.getInt(ARGUMENT_PREFERENCES_XML_ID, R.xml.default_tool_preferences)
        } catch (e: IllegalStateException) {
            R.xml.default_tool_preferences
        }
    }
}