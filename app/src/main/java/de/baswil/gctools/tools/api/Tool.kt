package de.baswil.gctools.tools.api

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment
import de.baswil.gctools.tools.api.defaults.ToolSettingsDefaultFragment
import java.io.Serializable

abstract class Tool : Serializable {
    @StringRes
    abstract fun getTitleId(): Int

    abstract fun createToolFragment(): Fragment

    // Preferences Methods
    open fun createPreferencesFragment(): Fragment? {
        return ToolSettingsDefaultFragment.newInstance(R.xml.default_tool_preferences)
    }

    open fun isPreferencesFragmentEnabled(): Boolean = true

    // Description Methods
    open fun createDescriptionFragment(): Fragment? {
        return ToolDescriptionDefaultFragment.newInstance(R.string.no_description)
    }

    open fun isDescriptionFragmentEnabled(): Boolean = true

    // General Functions
    open fun prepareFragmentArguments(fragment: Fragment, tool: Tool) {
        val bundle = Bundle()
        bundle.putSerializable("tool", tool)

        if (fragment.arguments == null) {
            fragment.arguments = bundle
        } else {
            fragment.requireArguments().putAll(bundle)
        }
    }
}