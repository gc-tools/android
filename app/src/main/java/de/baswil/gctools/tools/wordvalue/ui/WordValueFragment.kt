package de.baswil.gctools.tools.wordvalue.ui

import androidx.lifecycle.ViewModelProvider
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.onewayencodingtool.AbstractOneWayEncodingDefaultFragment


class WordValueFragment : AbstractOneWayEncodingDefaultFragment<WordValueViewModel>() {
    override fun createViewModel(): WordValueViewModel {
        return ViewModelProvider(this).get(WordValueViewModel::class.java)
    }

    override fun getPlainTextLabelId(): Int {
        return R.string.wordvalue_input_label
    }

    override fun getCipherTextLabelId(): Int {
        return R.string.wordvalue_output_label
    }
}
