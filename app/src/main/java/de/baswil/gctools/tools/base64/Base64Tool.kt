package de.baswil.gctools.tools.base64

import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment

class Base64Tool : Tool() {
    override fun getTitleId(): Int {
        return R.string.base64_title
    }

    override fun createToolFragment(): Fragment {
        return Base64Fragment()
    }

    override fun createPreferencesFragment(): Fragment? {
        return null
    }

    override fun isPreferencesFragmentEnabled() = false

    override fun createDescriptionFragment(): Fragment {
        return ToolDescriptionDefaultFragment.newInstance(
            R.string.base64_description1,
            R.string.base64_description2,
            R.string.base64_description3
        )
    }
}