package de.baswil.gctools.tools.base64

import androidx.lifecycle.ViewModelProvider
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragment

class Base64Fragment : AbstractTwoWayEncodingDefaultFragment<Base64ViewModel>() {

    override fun createViewModel(): Base64ViewModel {
        return ViewModelProvider(this).get(Base64ViewModel::class.java)
    }

    override fun getCipherTextLabelId(): Int {
        return R.string.base64_base64_text
    }
}