package de.baswil.gctools.tools.api.defaults.twowayencodingtool

import androidx.lifecycle.LiveData
import de.baswil.gctools.tools.api.defaults.onewayencodingtool.OneWayEncodingDefaultFragmentViewModel

interface TwoWayEncodingDefaultFragmentViewModel : OneWayEncodingDefaultFragmentViewModel {
    fun toggleMode()
    fun getMode(): LiveData<String>
}