package de.baswil.gctools.tools.wordvalue

import androidx.fragment.app.Fragment
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.Tool
import de.baswil.gctools.tools.api.defaults.ToolDescriptionDefaultFragment
import de.baswil.gctools.tools.api.defaults.ToolSettingsDefaultFragment
import de.baswil.gctools.tools.wordvalue.ui.WordValueFragment

class WordValueTool : Tool() {
    override fun getTitleId(): Int {
        return R.string.wordvalue_title
    }

    override fun createToolFragment(): Fragment {
        return WordValueFragment()
    }

    override fun createPreferencesFragment(): Fragment {
        return ToolSettingsDefaultFragment.newInstance(R.xml.wordvalue_preferences)
    }

    override fun createDescriptionFragment(): Fragment {
        return ToolDescriptionDefaultFragment.newInstance(
            R.string.wordvalue_description1,
            R.string.wordvalue_description2,
            R.string.wordvalue_description3,
        )
    }
}