package de.baswil.gctools.tools.romannumber

import java.util.*

class RomanNumberDecoder {
    private val romanTranslateList: List<Pair<Int, String>> by lazy {
        listOf(
            Pair(1000, "M"),
            Pair(900, "CM"),
            Pair(500, "D"),
            Pair(400, "CD"),
            Pair(100, "C"),
            Pair(90, "XC"),
            Pair(50, "L"),
            Pair(40, "XL"),
            Pair(10, "X"),
            Pair(9, "IX"),
            Pair(5, "V"),
            Pair(4, "IV"),
            Pair(1, "I"),
        )
    }

    fun decode(string: String?): String {
        if (string == null || string.trim().isEmpty()) {
            return ""
        }

        var input: String = string.trim().toUpperCase(Locale.ROOT)
        if (!input.matches("^M{0,4}(CM|CD|D?C{0,4})(XC|XL|L?X{0,4})(IX|IV|V?I{0,4})$".toRegex())) {
            return ""
        }

        var result = 0
        for (translatePair in romanTranslateList) {
            var count = 0
            while (input.startsWith(translatePair.second)) {
                result += translatePair.first
                input = input.substring(translatePair.second.length)
                count++
            }

            if (input.isEmpty()) {
                break
            }
        }

        return result.toString()
    }
}