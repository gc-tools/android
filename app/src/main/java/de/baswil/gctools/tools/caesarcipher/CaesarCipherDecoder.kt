package de.baswil.gctools.tools.caesarcipher

class CaesarCipherDecoder : CaesarCipherBaseCoder() {
    fun decode(string: String?, key: Int): String {
        return code(string, -key)
    }
}