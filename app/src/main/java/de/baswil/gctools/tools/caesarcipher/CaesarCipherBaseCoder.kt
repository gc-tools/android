package de.baswil.gctools.tools.caesarcipher

abstract class CaesarCipherBaseCoder {
    protected fun code(string: String?, key: Int): String {
        if (string == null || string.isEmpty()) {
            return ""
        }

        return string
            .map { char ->
                if (char in 'A'..'Z' || char in 'a'..'z') {
                    val upper = char in 'A'..'Z'
                    var result = char + (key % 26)
                    if (upper) {
                        if (result < 'A') {
                            result += 26
                        } else if (result > 'Z') {
                            result -= 26
                        }
                    } else {
                        if (result < 'a') {
                            result += 26
                        } else if (result > 'z') {
                            result -= 26
                        }
                    }

                    result
                } else {
                    char
                }
            }
            .joinToString("")
    }
}