package de.baswil.gctools.tools.morsecode

import androidx.lifecycle.ViewModelProvider
import de.baswil.gctools.R
import de.baswil.gctools.tools.api.defaults.twowayencodingtool.AbstractTwoWayEncodingDefaultFragment


class MorseCodeFragment : AbstractTwoWayEncodingDefaultFragment<MorseCodeViewModel>() {
    override fun createViewModel(): MorseCodeViewModel {
        return ViewModelProvider(this).get(MorseCodeViewModel::class.java)
    }

    override fun getCipherTextLabelId(): Int {
        return R.string.morsecode_morsecode_text
    }
}