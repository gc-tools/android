package de.baswil.gctools

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import de.baswil.gctools.utils.preferances.setDarkTheme

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        setDarkTheme(this)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        this.navController = (navHostFragment as NavHostFragment).navController
        this.navController.addOnDestinationChangedListener { _, _, _ ->
            val inputMethodService =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val windowToken = findViewById<View>(android.R.id.content).rootView.windowToken;
            inputMethodService.hideSoftInputFromWindow(windowToken, 0)
        }

        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}